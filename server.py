from websocket_server import WebsocketServer
import time
import threading

ENABLE_IMU = True

timeDiffDict = {}

if ENABLE_IMU:
    from lpmslib37 import LpmsME
    from lpmslib37 import lputils
    port = 'COM6'
    baudrate = 115200
    lpmsSensor = LpmsME.LpmsME(port, baudrate)


TAG = "NCTU-PROJECTX"

def get_timestamp():
    return int(round(time.time() * 1000))


def connect_sensor():
    lputils.logd(TAG, "Connecting sensor")
    if lpmsSensor.connect():
        lputils.logd(TAG, "Connected")
        get_config_register()

def disconnect_sensor():
    lputils.logd(TAG, "Disconnecting sensor")
    lpmsSensor.disconnect()
    lputils.logd(TAG, "Disconnected")
        
def get_config_register():
    config_reg = lpmsSensor.get_config_register()
    print(config_reg)
    
    
def pretty_print_sensor_data(sensor_data):
    j = 25
    d = '.'
    print("IMU ID:".ljust(j, d), sensor_data[0])
    print("TimeStamp:".ljust(j, d), sensor_data[1])
    print("Frame Counter:".ljust(j, d), sensor_data[2])
    print("Temperature:".ljust(j, d), sensor_data[3])
    print("Acc:".ljust(j, d), ['%+.3f' % f for f in sensor_data[4]])
    print("Gyr:".ljust(j, d), ['%+.3f' % f for f in sensor_data[5]])
    print("Mag:".ljust(j, d), ['%+.3f' % f for f in sensor_data[6]])
    print("Quat:".ljust(j, d), ['%+.3f' % f for f in sensor_data[7]])
    print("Euler:".ljust(j, d), ['%+.3f' % f for f in sensor_data[8]])
    print("LinAcc:".ljust(j, d), ['%+.3f' % f for f in sensor_data[9]])

# Called for every client connecting (after handshake)
def new_client(client, server):
    print("New client connected and was given id %d" % client['id'])
    #server.send_message_to_all("Hey all, a new client has joined us")

# Called for every client disconnecting
def client_left(client, server):
    print("Client(%d) disconnected" % client['id'])

# Called when a client sends a message
def message_received(client, server, message):
    print('MESSAGE RECEIVED!')
    if message.startswith("DATA"):
        millis = int(round(time.time() * 1000))
        data = message.split("|")
        print("\nTimestamp from Server:"+data[1]+
            "\nClient:"+data[2]+
            "\n"+data[3]+
            "\n"+data[4]+
            "\n"+data[5]+
            "\nCurrent Timestamp:"+str(millis)+
            "\nTime Diff:"+str(timeDiffDict[data[2]]))
    elif message.startswith("SYNC"):
        millis = int(round(time.time() * 1000))
        data = message.split("|")
        diff = millis-int(data[2])
        print("\nClient:"+data[1]+
              "\nTimestamp from client:"+data[2]+
              "\nCurrent Timestamp:"+str(millis)+
              "\nTimestamp Diff:"+str(diff))
        timeDiffDict[data[1]] = diff

def wait_input():
    while True:
        choice = input("> ")
        if choice == 'q' :
            if ENABLE_IMU:
                disconnect_sensor()
                lputils.logd(TAG, "bye")
            print('bye')
            break
        elif choice == 'd':
            millis = get_timestamp()
            ## get imu data
            if ENABLE_IMU:
                sensor_data = lpmsSensor.get_stream_data()
                pretty_print_sensor_data(sensor_data)
            ## send trigger message to all client
            print('DATA')
            server.send_message_to_all("DATA")
        elif choice == 't':
            print('SYNC')
            server.send_message_to_all("SYNC")
            

PORT=9001
server = WebsocketServer(PORT, host='0.0.0.0')
server.set_fn_new_client(new_client)
server.set_fn_client_left(client_left)
server.set_fn_message_received(message_received)

triggerThread = threading.Thread(target=wait_input, args=())
triggerThread.start()

if ENABLE_IMU:
    connect_sensor()

server.run_forever()

