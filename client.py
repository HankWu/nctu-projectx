import websocket
import threading
import time
import sys

id = "unknown"
ip = "localhost"
global imageProcessThread
imageProcessRunning = True
frameCount = 0
x = 0
y = 0
z = 0
frameTimeStamp = 0


def get_timestamp():
    return int(round(time.time() * 1000))

def image_processing():
    global frameCount, imageProcessRunning, frameTimeStamp, x, y, z
    while imageProcessRunning:
        frameTimeStamp = get_timestamp()
        print("process {} frame on {}".format(frameCount, frameTimeStamp))
        frameCount = frameCount+1
        x = y = z = frameCount
        time.sleep(1)
  
def on_message(ws, message):
    print("Received from server:"+message)
    global frameCount, frameTimeStamp, x, y, z
    if 'DATA' in message:
        print('DATA')
        data="X={}|Y={}|Z={}".format(x,y,z)
        msg = "DATA|"+str(frameTimeStamp)+"|"+id+"|"+data
        ws.send(msg)
    elif 'SYNC' in message:
        print('SYNC TIME')
        msg = "SYNC|"+id+"|"+str(get_timestamp())
        ws.send(msg)
    else:
        print('ELSE')


def on_error(ws, error):
    print(error)


def on_close(ws):
    global imageProcessRunning
    imageProcessRunning = False
    print("### closed ###")
    print("### Is Sever still running? ####");


def on_open(ws):
    global imageProcessThread
    print("### on open ###")

    print("### image process thread running!! ###")        
    imageProcessThread = threading.Thread(target=image_processing, args=())
    imageProcessThread.start()
    #def run(*args):
    #    for i in range(100):
            # send the message, then wait
            # so thread doesn't exit and socket
            # isn't closed
    #        ws.send("Hello %d" % i)
    #        time.sleep(1)

    #    time.sleep(1)
    #    ws.close()
    #    print("Thread terminating...")

    #Thread(target=run).start()


if __name__ == "__main__":

    websocket.enableTrace(False)
    
    if len(sys.argv)<3:
        sys.exit('ERROR, Please try to using the following command:\npython client.py ClientX "192.168.xxx.xxx"')

    id = sys.argv[1]
    ip = sys.argv[2]
    
    host = "ws://{}:9001".format(ip)
    #if len(sys.argv) < 2:
    #    host = "ws://localhost:9001"
    #else:
    #    host = sys.argv[1]
        
    print("Connect to {}".format(host))
    
    ws = websocket.WebSocketApp(host,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open
    ws.run_forever()